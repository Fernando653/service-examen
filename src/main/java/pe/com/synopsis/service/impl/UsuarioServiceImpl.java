package pe.com.synopsis.service.impl;

import java.util.List;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.com.synopsis.beans.Usuario;
import pe.com.synopsis.beans.response.ListaUsuarioResponse;
import pe.com.synopsis.beans.response.UsuarioResponse;
import pe.com.synopsis.dao.UsuarioDao;
import pe.com.synopsis.service.UsuarioService;

@Service
public class UsuarioServiceImpl implements UsuarioService {

	private static final Logger logger = LogManager.getLogger(UsuarioServiceImpl.class);
	
	@Autowired
	UsuarioDao usuarioDao;

	@Override
	public UsuarioResponse obtenerDatosUsuario(int codigo) 
	{
		
		logger.info("Ingreso al metodo obtenerDatosUsuario()");
		UsuarioResponse response = new UsuarioResponse();
		Optional<Usuario> u = usuarioDao.findById(codigo);
		
		if(u.isPresent()) 
		{
			response.setUsuario(u.get());
		}
		return response;
	}

	@Override
	public ListaUsuarioResponse listaUsuarios() {

		logger.info("Ingreso al metodo listaUsuarios()");
		
		ListaUsuarioResponse response = new ListaUsuarioResponse();
		
		List<Usuario> listaUsuario = usuarioDao.findAll();

		response.setListaUsuario(listaUsuario);
		
		return response;
	}

	@Override
	public Integer insertarUsuario(Usuario usuario) 
	{
		Usuario u = usuarioDao.save(usuario);
		int response = 0;
		if(u!=null)
		{
			response = 1;
		}
		return response;
	}

	@Override
	public Integer actualizarUsuario(Usuario usuario) {
		int response = insertarUsuario(usuario);
		return response;
	}

	@Override
	public Integer eliminarUsuario(int codigo) 
	{
		int response = 0;
		
		try
		{
			usuarioDao.deleteById(codigo);
			response = 1;
		}
		catch (Exception e) 
		{
			logger.error(e);
		}
		
		return response;
	}

}
